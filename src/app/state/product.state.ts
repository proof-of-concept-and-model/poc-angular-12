export enum ProductActionTypes {

  GET_ALL_PRODUCTS,
  GET_SELECTED_PRODUCTS,
  GET_AVAILABLE_PRODUCTS,
  SEARCH_PRODUCTS,
  NEW_PRODUCT,
  SELECT_PRODUCT,
  EDIT_PRODUCT,
  DELETE_PRODUCT,
  ADDED_PRODUCT,
  UPDATED_PRODUCT,

}

export interface ActionEvent {
  type : ProductActionTypes,
  payload? : any,
}

export enum DataStateEnum {
  LOADING,
  LOADED,
  ERROR
}

export interface AppDataState<T> {
  dataState?:DataStateEnum,
  data?:T,
  errorMessage?:string
}
