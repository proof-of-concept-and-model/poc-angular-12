import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../../services/products.service";
import {Product} from "../../model/product.model";
import {catchError, map, startWith} from "rxjs/operators";
import {Observable, of} from "rxjs";
import {ActionEvent, AppDataState, DataStateEnum, ProductActionTypes} from "../../state/product.state";
import {Router} from "@angular/router";
import {EventDriverService} from "../../services/event.driver.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products$: Observable<AppDataState<Product[]>> | null=null;
  readonly DataStateEnum=DataStateEnum;

  constructor(private productService:ProductsService, private router:Router,
              private eventDriverService : EventDriverService) { }

  ngOnInit(): void {
    this.eventDriverService.sourceEventSubjectObservable.subscribe((actionEvent : ActionEvent)=>{
      this.onActionEvent(actionEvent);
    });
  }

  onGetAllProducts() {
    console.log("Start")
    this.products$ = this.productService.getAllProducts()
      .pipe(
        map(data =>{
          return ({dataState:DataStateEnum.LOADED, data: data})
        }),
        startWith({dataState:DataStateEnum.LOADING}),
        catchError(err =>of({dataState:DataStateEnum.ERROR, errorMessage:err.message}))
      );
  }

  onGetSelectedProducts() {
    console.log("Start")
    this.products$ = this.productService.getSelectedProducts()
      .pipe(
        map(data =>{
          return ({dataState:DataStateEnum.LOADED, data: data})
        }),
        startWith({dataState:DataStateEnum.LOADING}),
        catchError(err =>of({dataState:DataStateEnum.ERROR, errorMessage:err.message}))
      );
  }

  onGetAvailableProducts() {
    console.log("Start")
    this.products$ = this.productService.getAvailableProducts()
      .pipe(
        map(data =>{
          return ({dataState:DataStateEnum.LOADED, data: data})
        }),
        startWith({dataState:DataStateEnum.LOADING}),
        catchError(err =>of({dataState:DataStateEnum.ERROR, errorMessage:err.message}))
      );
  }

  onSearch(value: any) {
    console.log("Start")
    this.products$ = this.productService.getSearchProducts(value.keyword)
      .pipe(
        map(data =>{
          return ({dataState:DataStateEnum.LOADED, data: data})
        }),
        startWith({dataState:DataStateEnum.LOADING}),
        catchError(err =>of({dataState:DataStateEnum.ERROR, errorMessage:err.message}))
      );
  }

  onSelect(p:Product) {
    this.productService.select(p)
      .subscribe(data=>{
        p.selected=data.selected;
      })

  }

  onDelete(p:Product) {
    let v=confirm("Etes-vous sûre ?");
    if (v==true)
      this.productService.deleteProduct(p)
        .subscribe(data=>{
          this.onGetAllProducts();
        })
  }

  onNewProducts() {
    this.router.navigateByUrl("/newProduct")
  }

  onEdit(p: Product) {
    this.router.navigateByUrl("/editProduct/" + p.id)
  }

  onActionEvent($event : ActionEvent) {
    switch ($event.type) {
      case ProductActionTypes.GET_ALL_PRODUCTS : this.onGetAllProducts(); break;
      case ProductActionTypes.GET_SELECTED_PRODUCTS : this.onGetSelectedProducts(); break;
      case ProductActionTypes.GET_AVAILABLE_PRODUCTS : this.onGetAvailableProducts(); break;
      case ProductActionTypes.SEARCH_PRODUCTS : this.onSearch($event.payload); break;
      case ProductActionTypes.NEW_PRODUCT : this.onNewProducts(); break;
      case ProductActionTypes.SELECT_PRODUCT : this.onSelect($event.payload); break;
      case ProductActionTypes.DELETE_PRODUCT : this.onDelete($event.payload); break;
      case ProductActionTypes.EDIT_PRODUCT : this.onEdit($event.payload); break;
    }
  }
}
